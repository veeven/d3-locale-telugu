/*
  Telugu locale definition for D3.js

  See: https://github.com/mbostock/d3/wiki/Localization#d3_locale
*/
var telugu = d3.locale({
  "decimal": ".",
  "thousands": ",",
  "grouping": [3,2,2,2,2],
  "currency": ["₹", ""],
  "dateTime": "%a %b %e %X %Y",
  "date": "%m/%d/%Y",
  "time": "%H:%M:%S",
  "periods": ["తొలి", "మలి"], // కొత్త పదాల ప్రయోగం. చూడండి: http://veeven.wordpress.com/2014/05/30/am-pm-in-telugu/
  "days": ["ఆదివారం", "సోమవారం", "మంగళవారం", "బుధవారం", "గురువారం", "శుక్రవారం", "శనివారం"],
  "shortDays": ["ఆది", "సోమ", "మంగళ", "బుధ", "గురు", "శుక్ర", "శని"],
  "months": ["జనవరి", "ఫిబ్రవరి", "మార్చి", "ఏప్రిల్", "మే", "జూన్", "జూలై", "ఆగస్టు", "సెప్టెంబరు", "అక్టోబరు", "నవంబరు", "డిసెంబరు"],
  "shortMonths": ["జన", "ఫిబ్ర", "మార్చి", "ఏప్రి", "మే", "జూన్", "జూలై", "ఆగ", "సెప్టెం", "అక్టో", "నవం", "డిసెం"]
});
